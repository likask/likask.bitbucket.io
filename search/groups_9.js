var searchData=
[
  ['manages_20boundary_20conditions_26181',['Manages boundary conditions',['../group__bc__manager.html',1,'']]],
  ['mass_20element_26182',['Mass Element',['../group__convective__mass__elem.html',1,'']]],
  ['minimal_20surface_20area_26183',['Minimal surface area',['../group__minimal__surface__area.html',1,'']]],
  ['mofem_26184',['MoFEM',['../group__mofem.html',1,'']]],
  ['mofem_5flog_5fmanager_26185',['Mofem_log_manager',['../group__mofem__log__manager.html',1,'']]],
  ['matrix_20manager_26186',['Matrix Manager',['../group__mofem__mat__interface.html',1,'']]],
  ['meshsetsmanager_26187',['MeshsetsManager',['../group__mofem__meshset__mng.html',1,'']]],
  ['mix_20transport_20element_26188',['Mix transport element',['../group__mofem__mix__transport__elem.html',1,'']]],
  ['meshrefinement_26189',['MeshRefinement',['../group__mofem__refiner.html',1,'']]]
];
