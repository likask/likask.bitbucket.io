var searchData=
[
  ['plasticity_26307',['Plasticity',['../jup_plasticity.html',1,'jup_tutorial_plastic_problem']]],
  ['plate_20with_20hole_26308',['Plate with hole',['../jup_plate_with_hole.html',1,'jup_tutorial_plastic_problem']]],
  ['password_20and_20login_26309',['Password and login',['../md__mofem_install_jupyter_lukasz_mofem_install_mofem-cephas_jupyter_templates_login_notes.html',1,'']]],
  ['photon_20diffusion_26310',['Photon diffusion',['../md_tutorials_scl-10_README.html',1,'']]],
  ['pluto_26311',['pluto',['../md_tutorials_scl-8_pluto.html',1,'']]],
  ['periodic_20boundary_20conditions_20with_20nitsche_27s_20method_26312',['Periodic boundary conditions with Nitsche&apos;s method',['../nitsche_periodic.html',1,'runningprograms']]],
  ['programs_26313',['Programs',['../runningprograms.html',1,'']]],
  ['plasticity_20_28user_20example_29_26314',['Plasticity (User example)',['../small_strain_plasticity_plate_with_hole.html',1,'runningprograms']]]
];
