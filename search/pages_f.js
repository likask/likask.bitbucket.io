var searchData=
[
  ['scl_2d6_3a_20heat_20equation_26335',['SCL-6: Heat equation',['../basic_tutorials_heat_equation.html',1,'']]],
  ['scl_2d0_3a_20least_20square_20approximaton_26336',['SCL-0: Least square approximaton',['../basic_tutorials_least_square_approximation.html',1,'']]],
  ['scl_2d5_3a_20minimal_20surface_20equation_26337',['SCL-5: Minimal surface equation',['../basic_tutorials_minimal_surface_equation.html',1,'']]],
  ['scl_2d1_3a_20poisson_27s_20equation_20_28homogeneous_20bc_29_26338',['SCL-1: Poisson&apos;s equation (homogeneous BC)',['../basic_tutorials_poisson_homogeneous.html',1,'']]],
  ['scl_2d3_3a_20poisson_27s_20equation_20_28lagrange_20multiplier_29_26339',['SCL-3: Poisson&apos;s equation (Lagrange multiplier)',['../basic_tutorials_poisson_lagrange_multiplier.html',1,'']]],
  ['scl_2d2_3a_20poisson_27s_20equation_20_28non_2dhomogeneous_20bc_29_26340',['SCL-2: Poisson&apos;s equation (non-homogeneous BC)',['../basic_tutorials_poisson_nonhomogeneous.html',1,'']]],
  ['scl_2d4_3a_20nonlinear_20poisson_27s_20equation_26341',['SCL-4: Nonlinear Poisson&apos;s equation',['../basic_tutorials_poisson_nonlinear.html',1,'']]],
  ['scl_2d7_3a_20wave_20equation_26342',['SCL-7: Wave equation',['../basic_tutorials_wave_equation.html',1,'']]],
  ['search_26343',['Search',['../google_search.html',1,'']]],
  ['scl_2d8_3a_20radiation_20boundary_20conditions_26344',['SCL-8: Radiation boundary conditions',['../jup_radiation_pluto.html',1,'running_code']]],
  ['selected_20publications_20featuring_20results_20obtained_20using_20_25mofem_26345',['Selected publications featuring results obtained using %MoFEM',['../publications.html',1,'']]],
  ['soap_20film_20spanned_20on_20wire_26346',['Soap film spanned on wire',['../sope_film.html',1,'tutorials']]],
  ['scl_2d11_3a_20discontinuous_20galerkin_20for_20poisson_20problem_26347',['SCL-11: Discontinuous Galerkin for Poisson problem',['../tutorial_dg_poisson.html',1,'']]],
  ['scl_2d9_3a_20heat_20method_26348',['SCL-9: Heat method',['../tutorial_heat_method.html',1,'']]],
  ['scl_2d10_3a_20photon_20diffusion_26349',['SCL-10: Photon diffusion',['../tutorials_photon_diffusion.html',1,'']]]
];
